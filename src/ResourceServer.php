<?php

/**
 * OAuth 2.0 Resource Server
 *
 * @package     league/oauth2-server
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 * @link        https://github.com/thephpleague/oauth2-server
 */

namespace League\OAuth2\Server;

use Illuminate\Support\Facades\Log;
use League\OAuth2\Server\Entity\AccessTokenEntity;
use League\OAuth2\Server\Exception\AccessDeniedException;
use League\OAuth2\Server\Exception\InvalidRequestException;
use League\OAuth2\Server\Storage\AccessTokenInterface;
use League\OAuth2\Server\Storage\ClientInterface;
use League\OAuth2\Server\Storage\ScopeInterface;
use League\OAuth2\Server\Storage\SessionInterface;
use League\OAuth2\Server\TokenType\Bearer;
use League\OAuth2\Server\TokenType\MAC;

/**
 * OAuth 2.0 Resource Server
 */
class ResourceServer extends AbstractServer
{
    /**
     * The access token
     *
     * @var \League\OAuth2\Server\Entity\AccessTokenEntity
     */
    protected $accessToken;

    /**
     * The query string key which is used by clients to present the access token (default: access_token)
     *
     * @var string
     */
    protected $tokenKey = 'access_token';

    /**
     * Initialise the resource server
     *
     * @param \League\OAuth2\Server\Storage\SessionInterface     $sessionStorage
     * @param \League\OAuth2\Server\Storage\AccessTokenInterface $accessTokenStorage
     * @param \League\OAuth2\Server\Storage\ClientInterface      $clientStorage
     * @param \League\OAuth2\Server\Storage\ScopeInterface       $scopeStorage
     *
     * @return self
     */
    public function __construct(
        SessionInterface $sessionStorage,
        AccessTokenInterface $accessTokenStorage,
        ClientInterface $clientStorage,
        ScopeInterface $scopeStorage
    ) {

        ## 构造
        $debugArr = [
            '__construct' => 'ResourceServer',
            'sessionStorage' => $sessionStorage ? get_class($sessionStorage) : 0,
            'accessTokenStorage' => $accessTokenStorage ? get_class($accessTokenStorage) : 0,
            'clientStorage' => $clientStorage ? get_class($clientStorage) : 0,
            'scopeStorage' => $scopeStorage ? get_class($scopeStorage) : 0,
            'line' => __LINE__,
            'method' => __METHOD__,
        ];
        // Log::channel('debug')->debug(print_r($debugArr, true));

        $this->setSessionStorage($sessionStorage);
        $this->setAccessTokenStorage($accessTokenStorage);
        $this->setClientStorage($clientStorage);
        $this->setScopeStorage($scopeStorage);

        // Set Bearer as the default token type
        $this->setTokenType(new Bearer());


        parent::__construct();

        return $this;
    }

    /**
     * Sets the query string key for the access token.
     *
     * @param string $key The new query string key
     *
     * @return self
     */
    public function setIdKey($key)
    {
        $this->tokenKey = $key;

        return $this;
    }

    /**
     * Gets the access token
     *
     * @return \League\OAuth2\Server\Entity\AccessTokenEntity
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Checks if the access token is valid or not
     *
     * @param bool                                                $headerOnly Limit Access Token to Authorization header
     * @param \League\OAuth2\Server\Entity\AccessTokenEntity|null $accessToken Access Token
     *
     * @throws \League\OAuth2\Server\Exception\AccessDeniedException
     * @throws \League\OAuth2\Server\Exception\InvalidRequestException
     *
     * @return bool
     */
    public function isValidRequest($headerOnly = true, $accessToken = null)
    {
        $debugArr = [
            'line' => __LINE__,
            'method' => __METHOD__,
            'accessToken' => $accessToken ? 'accessToken isset' : 'accessToken empty',
        ];
        // Log::channel('debug')->debug(print_r($debugArr, true));
        $accessTokenString = ($accessToken !== null)
            ? $accessToken
            : $this->determineAccessToken($headerOnly);

        // Set the access token
        $this->accessToken = $this->getAccessTokenStorage()->get($accessTokenString);
        $debugArr = [
            'line' => __LINE__,
            'method' => __METHOD__,
            'accessToken' => $this->accessToken ? '$this->accessToken isset' : '$this->accessToken empty',
            'accessTokenString' => $accessTokenString ? 'accessTokenString isset ' : 'accessTokenString empty',
        ];
        // Log::channel('debug')->debug(print_r($debugArr, true));

        // Ensure the access token exists
        if (!$this->accessToken instanceof AccessTokenEntity) {
            $errMsg = "授权失败: 该access_token记录不存在";
            throw new AccessDeniedException($errMsg);
        }

        // Check the access token hasn't expired
        // Ensure the auth code hasn't expired
        if ($this->accessToken->isExpired() === true) {
            $errMsg = "授权失败: 该access_token已过期";
            throw new AccessDeniedException($errMsg);
        }

        return true;
    }

    /**
     * Reads in the access token from the headers
     *
     * @param bool $headerOnly Limit Access Token to Authorization header
     *
     * @throws \League\OAuth2\Server\Exception\InvalidRequestException Thrown if there is no access token presented
     *
     * @return string
     */
    public function determineAccessToken($headerOnly = false)
    {
        $debugArr = [
            'line' => __LINE__,
            'method' => __METHOD__,
            'getRequest-Authorization' => $this->getRequest()->headers->get('Authorization'),
            'tokenKey' => $this->tokenKey,
            'getRequest-query-tokenKey' => $this->getRequest()->query->get($this->tokenKey),
            'getRequest-request-tokenKey' => $this->getRequest()->request->get($this->tokenKey),
        ];
        // Log::channel('debug')->debug(print_r($debugArr, true));

        if ($this->getRequest()->headers->get('Authorization') !== null) {
            $accessToken = $this->getTokenType()->determineAccessTokenInHeader($this->getRequest());
        } elseif ($headerOnly === false && (!$this->getTokenType() instanceof MAC)) {
            $accessToken = ($this->getRequest()->server->get('REQUEST_METHOD') === 'GET')
                ? $this->getRequest()->query->get($this->tokenKey)
                : $this->getRequest()->request->get($this->tokenKey);
        }
        $debugArr = [
            'line' => __LINE__,
            'method' => __METHOD__,
            'accessToken' => $accessToken,
        ];
        // Log::channel('debug')->debug(print_r($debugArr, true));

        if (empty($accessToken)) {
            throw new InvalidRequestException('access token');
        }

        return $accessToken;
    }
}
